"use strict"

let utilisateur = {
    nom: "Fleury, Philippe",
    dateNaissance: new Date (1982, 8, 22),
    sexe: "H",
    situationParticuliere: "non",
}
const dateAujourdHui = new Date()

let age = dateNaissanceA_M_J()

let nbJour = prompt("Combien de jours voulez-vous analyser?")
nbJour = parseInt(nbJour)
let alcool = 0

const journeesAlcool = []
for (let i = 1; i <= nbJour; i++) {
    ajoutDonneeTableauAlcool()
}

var { maxAlcoolSemaine, maxAlcoolJour } = maxRecommandationConsomationAlcool()

let recommandationAlcoolRespectee = "vrai"

let dateAujourdHuiAffiche = dateJJ_MM_AAAA_a_HH_mm_SS()
console.log(dateAujourdHuiAffiche)
console.log(utilisateur.nom)

console.log("Âge:", age, "an(s)")
console.log("Consommation alcool:", alcool)

var moyenne = moyenneConsommationAlcool().toFixed(2)
console.log("Moyenne par jour:", moyenne)

if (journeesAlcool.length < 7) {
    var sommeHebdo = alcool
}
else {
    var sommeHebdo = moyenneDepasse7()
}

console.log("Consommation sur une semaine:", sommeHebdo, "Recommandation:", maxAlcoolSemaine)

let max = verificationRespectConsomationAlcool()
console.log("Maximum en une journée:", max, "Recommandation:", maxAlcoolJour)

let nbJoursExcedant = nbJour > maxAlcoolJour
let ratio = (nbJoursExcedant / nbJour) * 100
console.log("Ratio de de journées exédants:", ratio.toFixed(2), "%")

let nbJoursZero = 0
for (let i = 0; i <= journeesAlcool.length; i++) {
    compterNbJour0(i)
}
let ratio2 = (nbJoursZero / nbJour) * 100
console.log("Ratio de journées sans alcool:", ratio2.toFixed(2), "%")

afficherUtilisateurRespectRecommandation()

/**
 * Calculer la date de naissance à partir de AAAA - MM - JJ
 *
 * @return {number} Donne l'âge de l'utilisateur 
 */
function dateNaissanceA_M_J() {
    var dateAujourdHui = new Date()
    var diffAnnee = dateAujourdHui - utilisateur.dateNaissance
    var age = diffAnnee / (1000 * 60 * 60 * 24 * 365)
    age = Math.floor(age)
    return age
}
/**
 * Sert à faire entrer le nombre de consommation de boisson alcoolique par jour selon le nombre de jour entré par l'utilisateur
 *
 * @return {Object} le nombre total d'alcool consommé et le nombre d'alcool comsommé par journée
 */
function ajoutDonneeTableauAlcool() {
    let nbConsomationAlcool = prompt("Entrez le nombre de consommation d'alcool")
    nbConsomationAlcool = parseInt(nbConsomationAlcool)
    journeesAlcool.push(nbConsomationAlcool)
    alcool = alcool + nbConsomationAlcool
    return { alcool, nbConsomationAlcool}
}
/**
 * Sert à déterminer selon le sexe de l'utilisateur et si il à une condition particulière d'indiquer sa recommandation de consommation de boisson alcoolique
 *
 * @return {Object} Le maximum recommandé de consommation de boisson alcoolique par semaine et par jour
 */
function maxRecommandationConsomationAlcool() {
    if (utilisateur.situationParticuliere == "oui") {
        var maxAlcoolSemaine = 0
        var maxAlcoolJour = 0
    }
    else if (utilisateur.sexe == "H") {
        var maxAlcoolSemaine = 15
        var maxAlcoolJour = 3
    }
    else {
        var maxAlcoolSemaine = 10
        var maxAlcoolJour = 2
    }
    return { maxAlcoolSemaine, maxAlcoolJour }
}
/**
 * Convertir la date et l'heure en format  [Jour - Mois - Année | Heure : Minute : Seconde]
 *
 * @return {Object} La date d'aujourd'hui en format JJ/MM/AAAA à HH:mm:SS
 */
function dateJJ_MM_AAAA_a_HH_mm_SS() {
    let JJ = dateAujourdHui.getDate()
    let MM = dateAujourdHui.getMonth() + 1
    let AAAA = dateAujourdHui.getUTCFullYear()
    let HH = dateAujourdHui.getHours()
    let mm = dateAujourdHui.getMinutes()
    let SS = dateAujourdHui.getSeconds()
    if (JJ < 10) {
        JJ = `0${JJ}`
    }
    if (MM < 10) {
        MM = `0${MM}`
    }
    if (HH < 10) {
        HH = `0${HH}`
    }
    if (mm < 10) {
        mm = `0${mm}`
    }
    if (SS < 10) {
        SS = `0${SS}`
    }
    let dateAujourdHuiAffiche = `${JJ}/${MM}/${AAAA} à ${HH}:${mm}:${SS}`
    return dateAujourdHuiAffiche
}
/**
 * Faire la moyenne de consommation par jour sur une semaine et si plus de 7 jour
 *
 * @return {number} La moyenne de consommation de boisson alcoolique sur une semaine (7 jours)
 */
function moyenneConsommationAlcool() {
    moyenne = alcool / nbJour
    if (nbJour > 7) {
        moyenne = moyenne * 7 / nbJour
    }
    return moyenne
}
/**
 * Faire la moyenne de la consommation total d'alcool par semaine (7 jours) quand le nombre dépasse 7 jours (une semaine)
 *
 * @return {number} Le nombre de consommation de boisson alcoolique par semaine (7 jours)
 */
function moyenneDepasse7() {
    return journeesAlcool[0] + journeesAlcool[1] + journeesAlcool[2] + journeesAlcool[3] + journeesAlcool[4] + journeesAlcool[5] + journeesAlcool[6]
}
/**
 * Vérifier si le maximum recommander n'est pas dépassé soit par semaine et ou par jour
 *
 * @return {Boolean} retourn faux si recommandation n'est pas respecté par semaine et/ou par jour
 */
function verificationRespectConsomationAlcool() {
    if (sommeHebdo > maxAlcoolSemaine) {
        recommandationAlcoolRespectee = "faux"
    }
    let max = Math.max.apply(null, journeesAlcool)
    if (max > maxAlcoolJour) {
        recommandationAlcoolRespectee = "faux"
    }
    return max
}
/**
 * compter le nombre jour qui on un résultat de 0 pour en faire un cumule qui vas permettre de faire un moyenne des jours de non consomation d'alcool
 *
 * @param {Array} i fait le compte du nombre de fois que le chiffre 0 est présent dans le tableau journnesAlcool (dans nbJoursZero)
 */
function compterNbJour0(i) {
    if (journeesAlcool[i] == 0) {
        nbJoursZero++
    }
}
/**
 * indiquer à l'utilisateur si il respect les recommandation de consommation de boisson alcooloque
 *
 */
function afficherUtilisateurRespectRecommandation() {
    if (recommandationAlcoolRespectee == "vrai") {
        console.log("Vous respectez les recommandations")
    }
    else {
        console.log("Vous ne respectez pas les recommandations.")
    }
}
